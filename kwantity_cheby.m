function [kwantity, A, b, x] = kwantity_cheby(f, n, phi, a)
% kwantity_cheby(f,n,phi,a) calls RadialInterp_cheby(f,n,phi,a) and returns
% these results together with the relative residue
[A, b, x] = RadialInterp_cheby(f,n,phi,a);
kwantity = norm(A*(A\b)-b)/norm(b);
end

