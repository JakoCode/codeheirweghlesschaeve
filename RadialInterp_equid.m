function [A,b, x_interp] = RadialInterp_equid(f,n, phi,a)
% RadialInterp_equid(f,n,phi,a) returns a matrix A and vector b from (3)
% when x_i, i = 1,2,...,n are equidistant. f will be the function we want
% to approximate, phi a radial function and n the amount of interpolation
% points.
%   An extra input parameter 'a' was added since the phi used in this 
%   assignment only differ in the value for this 'a'. It was found easier
%   to just include this into the function call
x_interp = linspace(-1,1,n);
b = zeros(n,1);
% construct b by evaluating f in the n interpolation points
for h=1:n
    b(h,1) = feval(f,x_interp(h));
end
% construct elements on the diagonal of A
A = diag((feval(phi,a,-1)).*ones(1,n),0);
% construct all other elements of A. 
for lower = 1:1:n-1
    v = (feval(phi,a,(lower-1)*(x_interp(2)-x_interp(1))).*ones(1,n-lower));
    A = A +  diag(v,lower) + diag(v, -lower);
end
end
