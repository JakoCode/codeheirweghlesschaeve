function [L] = chol_A(A)
% chol_A(A) returns the L matrix of the Cholesky factorisation
% of a symmetrix and positive definitive matrix A given the matrix A
%   implementation makes use of the results in Opdracht 12
%   i chose to avoid the symsum library since it couldn't cope with 
%   zero-valued limits or negative limits (since if j = 0 -> j-1 = -1)
a_diag = diag(A);

dim = size(A);
n = dim(1);

L = zeros(length(a_diag),length(a_diag));

% loop over rows
for i = 1:n
    % loop over columns
    for j = 1:i
        % diagonal elements
        if j == i
            for k = 1:j-1
                L(i,j) = L(i,j)-(L(j,k)*L(j,k));
            end
            L(i,j) = sqrt(L(i,j)+A(i,j));
        % sub-diagonal elements
        else
            for k = 1:j-1
                L(i,j) = L(i,j)-(L(i,k)*L(j,k));
            end
            L(i,j) = (L(i,j)+A(i,j))/L(j,j);
        end 
    end
        
end


end

