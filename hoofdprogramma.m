%% Opdracht 5.
% create function handles:
pisin = @(x) sin(pi.*x);
expa = @(a,x) exp(-(a.*x).^2);
minsq = @(x) ones(1,length(x))./(ones(1,length(x)) + 25.*((x).^2));
% create 101 equidistant points:
x_e = linspace(-1,1,101);

% perform radial interpolation on functions and get relative residues:
[kwant01,A,b,x] = kwantity_equid(pisin,21,expa,2);
[kwant11,A1,b1,x1] = kwantity_equid(pisin,21,expa,3);
[kwant21,A2,b2,x2] = kwantity_equid(pisin,21,expa,10);
[kwant31,A3,b3,x3] = kwantity_equid(pisin,21,expa,100);
[kwant02,C,d,y] = kwantity_equid(minsq,21,expa,2);
[kwant12,C1,d1,y1] = kwantity_equid(minsq,21,expa,3);
[kwant22,C2,d2,y2] = kwantity_equid(minsq,21,expa,10);
[kwant32,C3,d3,y3] = kwantity_equid(minsq,21,expa,100);
%% Opdracht 9. (A.1)
% evaluate function and interpolant on points:
f_e_ps = feval(pisin,x_e);
fh_e_ps_2 = fhat_maker(expa, A\b,x,x_e,2);
fh_e_ps_3 = fhat_maker(expa, A1\b1,x1,x_e,3);
fh_e_ps_10 = fhat_maker(expa, A2\b2,x2,x_e,10);
fh_e_ps_100 = fhat_maker(expa, A3\b3,x3,x_e,100);
% plot results:
subplot(2,1,1);
plot(x_e, f_e_ps, x_e, fh_e_ps_2, x_e, fh_e_ps_3, x_e, fh_e_ps_10)
legend('exact f_1','a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('$$f_1$$ en $$\hat{f_1}$$ met parameter a', 'Interpreter', 'latex');
subplot(2,1,2);
plot(x_e, fh_e_ps_100);
legend('a = 100');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('$$\hat{f_1}$$ met parameter a', 'Interpreter', 'latex');
%% Opdracht 9. (A.2)
% calculate absolute error for function and interpolant:
[max_ps_2, mean_ps_2, abs_ps_2] = max_mean_abs(f_e_ps, fh_e_ps_2);
[max_ps_3, mean_ps_3, abs_ps_3] = max_mean_abs(f_e_ps, fh_e_ps_3);
[max_ps_10, mean_ps_10, abs_ps_10] = max_mean_abs(f_e_ps, fh_e_ps_10);
[max_ps_100, mean_ps_100, abs_ps_100] = max_mean_abs(f_e_ps, fh_e_ps_100);
% plot results:
subplot(2,1,1);
plot(x_e, abs_ps_2, x_e, abs_ps_3, x_e, abs_ps_10);
legend('a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('fout van $$|f_1-\hat{f_1}|$$ met a', 'Interpreter', 'latex');
subplot(2,1,2); 
plot(x_e, abs_ps_100);
legend('a = 100');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('fout van $$|f_1-\hat{f_1}|$$ met a', 'Interpreter', 'latex');
% place results in a table:
ps_matrix = [
    max_ps_2, mean_ps_2; 
    max_ps_3, mean_ps_3; 
    max_ps_10, mean_ps_10; 
    max_ps_100,mean_ps_100
];
ps_table = array2table(ps_matrix, 'VariableNames', {'Max_ps','Mean_ps'});
%% Opdracht 9. (A.3)
% evaluate function and interpolant on points:
f_e_ms = feval(minsq,x_e);
fh_e_ms_2 = fhat_maker(expa, C\d,y,x_e,2);
fh_e_ms_3 = fhat_maker(expa, C1\d1,y1,x_e,3);
fh_e_ms_10 = fhat_maker(expa, C2\d2,y2,x_e,10);
fh_e_ms_100 = fhat_maker(expa, C3\d3,y3,x_e,100);
% plot results:
subplot(2,1,1);
plot(x_e, f_e_ms, x_e, fh_e_ms_2, x_e, fh_e_ms_3, x_e, fh_e_ms_10);
legend('exact f_2','a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('$$f_2$$ en $$\hat{f_2}$$ met parameter a', 'Interpreter', 'latex');
subplot(2,1,2); 
plot(x_e, fh_e_ms_100);
legend('a = 100');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('$$\hat{f_2}$$ met parameter a', 'Interpreter', 'latex');
%% Opdracht 9. (A.4)
% calculate absolute error for function and interpolant:
[max_ms_2, mean_ms_2, abs_ms_2] = max_mean_abs(f_e_ms, fh_e_ms_2);
[max_ms_3, mean_ms_3, abs_ms_3] = max_mean_abs(f_e_ms, fh_e_ms_3);
[max_ms_10, mean_ms_10, abs_ms_10] = max_mean_abs(f_e_ms, fh_e_ms_10);
[max_ms_100, mean_ms_100, abs_ms_100] = max_mean_abs(f_e_ms, fh_e_ms_100);
% plot results:
subplot(2,1,1);
plot(x_e, abs_ms_2, x_e, abs_ms_3, x_e, abs_ms_10);
legend('a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('fout van $$|f_2-\hat{f_2}|$$ met a', 'Interpreter', 'latex');
subplot(2,1,2); 
plot(x_e, abs_ms_100);
legend('a = 100');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('fout van $$|f_2-\hat{f_2}|$$ met a', 'Interpreter', 'latex');
% place results in a table:
ms_matrix = [
    max_ms_2, mean_ms_2; 
    max_ms_3, mean_ms_3; 
    max_ms_10, mean_ms_10; 
    max_ms_100,mean_ms_100
];
ms_table = array2table(ms_matrix, 'VariableNames', {'Max_ms','Mean_ms'});
%% Opdracht 11.
x_cheby = zeros(1,101);
% create 101 Chebyshev points:
for i = 1:1:101
    x_cheby(1,i) = cos(pi*((i-1)/(101-1)));
end

% perform radial interpolation on functions:
[kwant03,E,f,z] = kwantity_cheby(pisin,21,expa,2);
[kwant13,E1,f1,z1] = kwantity_cheby(pisin,21,expa,3);
[kwant23,E2,f2,z2] = kwantity_cheby(pisin,21,expa,10);
[kwant04,G,h,s] = kwantity_cheby(minsq,21,expa,2);
[kwant14,G1,h1,s1] = kwantity_cheby(minsq,21,expa,3);
[kwant24,G2,h2,s2] = kwantity_cheby(minsq,21,expa,10);
%% Opdracht 11. (B.1)
% evaluate function and interpolant on points:
f_c_ps = feval(pisin,x_cheby);
fh_c_ps_2 = fhat_maker(expa, E\f,x,x_cheby,2);
fh_c_ps_3 = fhat_maker(expa, E1\f1,x1,x_cheby,3);
fh_c_ps_10 = fhat_maker(expa, E2\f2,x2,x_cheby,10);
% plot results:
plot(x_cheby, f_c_ps, x_cheby, fh_c_ps_2, x_cheby, fh_c_ps_3, x_cheby, fh_c_ps_10);
legend('exact f_1','a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('$$f_1$$ en $$\hat{f_1}$$ met parameter a', 'Interpreter', 'latex');
%% Opdracht 11. (B.2)
% calculate absolute error for function and interpolant:
[max_c_ps_2, mean_c_ps_2, abs_c_ps_2] = max_mean_abs(f_c_ps, fh_c_ps_2);
[max_c_ps_3, mean_c_ps_3, abs_c_ps_3] = max_mean_abs(f_c_ps, fh_c_ps_3);
[max_c_ps_10, mean_c_ps_10, abs_c_ps_10] = max_mean_abs(f_c_ps, fh_c_ps_10);
% plot results:
plot(x_cheby, abs_c_ps_2, x_cheby, abs_c_ps_3, x_cheby, abs_c_ps_10);
legend('a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('fout van $$|f_1-\hat{f_1}|$$ met a', 'Interpreter', 'latex');
% place results in a table:
ps_c_matrix = [
    max_c_ps_2, mean_c_ps_2; 
    max_c_ps_3, mean_c_ps_3; 
    max_c_ps_10, mean_c_ps_10; 
];
ps_c_table = array2table(ps_c_matrix, 'VariableNames', {'Max_ps','Mean_ps'});
%% Opdracht 11. (B.3)
% evaluate function and interpolant on points:
f_c_ms = feval(minsq,x_cheby);
fh_c_ms_2 = fhat_maker(expa, G\h,s,x_cheby,2);
fh_c_ms_3 = fhat_maker(expa, G1\h1,s1,x_cheby,3);
fh_c_ms_10 = fhat_maker(expa, G2\h2,s2,x_cheby,10);
% plot results:
plot(x_cheby, f_c_ms, x_cheby, fh_c_ms_2, x_cheby, fh_c_ms_3, x_cheby, fh_c_ms_10);
legend('exact f_2','a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('$$f_2$$ en $$\hat{f_2}$$ met parameter a', 'Interpreter', 'latex');
%% Opdracht 11. (B.4)
% calculate absolute error for function and interpolant:
[max_c_ms_2, mean_c_ms_2, abs_c_ms_2] = max_mean_abs(f_c_ms, fh_c_ms_2);
[max_c_ms_3, mean_c_ms_3, abs_c_ms_3] = max_mean_abs(f_c_ms, fh_c_ms_3);
[max_c_ms_10, mean_c_ms_10, abs_c_ms_10] = max_mean_abs(f_c_ms, fh_c_ms_10);
% plot results:
plot(x_cheby, abs_c_ms_2, x_cheby, abs_c_ms_3, x_cheby, abs_c_ms_10);
leg12 = legend('a = 2','a = 3','a = 10');
xlabel('$$-1 \leq x \leq 1$$', 'Interpreter', 'latex' );
ylabel('fout van $$|f_2-\hat{f_2}|$$ met a', 'Interpreter', 'latex');
% place results in a table:
ms_matrix = [
    max_c_ms_2, mean_c_ms_2; 
    max_c_ms_3, mean_c_ms_3; 
    max_c_ms_10, mean_c_ms_10; 
];
ms_c_table = array2table(ms_matrix, 'VariableNames', {'Max_ms','Mean_ms'});
%% opdracht 16
format long
rel_res_cell_inv_mult = zeros(8,1);
rel_res_cell_chol = zeros(8,1);
i = 1;
for n = [2,3,5,10,25,50,75,100]
    tmp = rand(n);
    A = tmp*tmp.';
    b = rand(n,1);
    % using inv(A)*b
    x_inv_mult = inv(A)*b;
    % calculate relative residues
    rel_res_inv_mult = norm(A*x_inv_mult-b)/norm(b);
    % using cholesky factorization
    L = chol_A(A);
    y = solve_Lb(L,b);
    w = solve_Ub(transpose(L),y);
    % calculate relative residues
    rel_res_chol = norm(A*w-b)/norm(b);
    rel_res_cell_inv_mult(i) = rel_res_inv_mult;
    rel_res_cell_chol(i) = rel_res_chol;
    i = i+1;
end  
% place results in a table:
rel_res_matrix = [
    2, rel_res_cell_inv_mult(1), rel_res_cell_chol(1);
    3, rel_res_cell_inv_mult(2), rel_res_cell_chol(2);
    5, rel_res_cell_inv_mult(3), rel_res_cell_chol(3);
    10, rel_res_cell_inv_mult(4), rel_res_cell_chol(4);
    25, rel_res_cell_inv_mult(5), rel_res_cell_chol(5);
    50, rel_res_cell_inv_mult(6), rel_res_cell_chol(6);
    75, rel_res_cell_inv_mult(7), rel_res_cell_chol(7);
    100 rel_res_cell_inv_mult(8), rel_res_cell_chol(8);
    ];
rel_res_table = array2table(rel_res_matrix, 'VariableNames', {'n', 'rel. res. inv(A)*b','rel. res. Cholesky'});