function [y] = solve_Lb(L,b)
% solve_Lb(L,b) performs backsubstitution for lower triangular matrix
%   analogous to solve_Ub.m 
n = length(b);
dim = size(L);
y = zeros(dim(1),1);

for k = 1:n
    y(k) = b(k);
    for i = 1:k-1
        y(k) = y(k) - (L(k,i)*y(i));
    end
    y(k) = y(k)/L(k,k);
end

end

