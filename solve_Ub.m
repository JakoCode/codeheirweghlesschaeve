function [y] = solve_Ub(U,b)
% solve_Ub(U,b) performs backsubstitution for an uppertriangular matrix 
%   according to pseudocode 3.1, page 45 of the book
n = length(b);
dim = size(U);
y = zeros(dim(1),1);

for k = n:-1:1
    y(k) = b(k);
    for i = k+1:n
        y(k) = y(k) - (U(k,i)*y(i));
    end
    y(k) = y(k)/U(k,k);
end

end

