function [fhat_v] = fhat_maker(phi,w,x_interp, x,a) 
% fhat_maker(phi,w,x_interp,x,a) evaluates the interpolant in a given 
% amount of interpolation points, inputs are the coefficients w, the radial
% function phi, x_interp the points in which we interpolate and x the
% points in which we wish to evaluate our interpolant.
%   implementation makes use of matrix-vector product, as instructed
%   not looping over all elements in the matrix
 interp_Matrix = zeros(length(x),length(x_interp));
 % construct matrix M according to points in x and phi
 for m=1:length(x)
    interp_Matrix(m,:) = feval(phi,a, x(m)-x_interp);
 end
 % perform matrix-vector product 
 fhat_v = interp_Matrix*w;
end

