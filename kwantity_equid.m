function [kwantity, A, b, x] = kwantity_equid(f, n, phi, a)
% kwantity_equid(f,n,phi,a) calls RadialInterp_equid(f,n,phi,a) and returns
% these results together with the relative residue
[A, b, x] = RadialInterp_equid(f,n,phi,a);
kwantity = norm(A*(A\b)-b)/norm(b);
end

