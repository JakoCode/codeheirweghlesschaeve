function [max_abs, mean_abs, abs_abs] = max_mean_abs(f,fh)
% max_mean_abs(f,fh) returns the max absolute, mean absolute and absolute
% error of fh (where fh is the interpolant of f)
%   see formula's in book
abs_abs = zeros(1, length(f));
for i = 1:1:length(abs_abs)
    abs_abs(1,i) = abs(f(i)-fh(i));
end
mean_abs = mean(abs_abs);
max_abs = max(abs_abs);
end

